const PORT = process.env.PORT||8000;

const express=require('express');
const { default: YoutubeTranscript } = require('youtube-transcript');

const captionsScraper = require('youtube-captions-scraper').getSubtitles;
const ytTranscript=require('youtube-transcript');



const app=express();


app.get('/',(req,res)  =>{
    

    res.json('This API retrieves Youtube Captions for any Specific Videos!');
})

//Version #1

app.get('/transcript', async (req, res) => {
    const videoId = req.query.videoId;
    try {
        const captions = await captionsScraper({videoID: videoId});
        res.json(captions);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});



//Version #2
app.get('/onlycaption', async (req, res) => {
    const videoId = req.query.videoId;
    try {YoutubeTranscript.fetchTranscript(videoId).then((response) => {
        const textArray=response.map(obj=>obj.text);
        const textString=textArray.join('').replace('\n', ' ');
        textString.replace('\n', ' ');

        
        res.json(textString)})
       // await ytTranscript.fetchTranscript(videoId).then((response) => res.json(response));
    //    res.json(captions);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

//Version #3

app.get('/transcript2', async (req, res) => {
    const videoId = req.query.videoId;
    try {YoutubeTranscript.fetchTranscript(videoId).then((response) => {res.json(response)})
       // await ytTranscript.fetchTranscript(videoId).then((response) => res.json(response));
    //    res.json(captions);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});






app.listen(PORT,()=>console.log('Server Starter at http://localhost:'+PORT));